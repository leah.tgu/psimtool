import datetime

from queue_model import Queue
import pandas as pd
import matplotlib.pyplot as plt


def generic_pivot_time(df, time_axes, aggfunc, values=None, reindex_range=None):
    index_attr = [getattr(df.index, i) for i in time_axes]
    if values is not None:
        df = df[values]
    pivot = pd.pivot_table(df, index=index_attr, aggfunc=aggfunc)

    if reindex_range is not None:
        if len(reindex_range) > 1:
            reindex_index = pd.MultiIndex.from_product(reindex_range, names=time_axes)
        else:
            reindex_index = reindex_range[0]

        return pivot.reindex(reindex_index)
    else:
        return pivot


def pivot_along_time(q: Queue, time_axes=None, reindex_range=None, xticks_args=None):
    r = q.get_replay()
    df = q.get_data()

    baseline_qlen_mean = df.qlen.mean()
    baseline_wait_mean = df.wait.dt.total_seconds().mean()

    en_pivot = generic_pivot_time(df.set_index('entry'), time_axes, aggfunc=[pd.Series.mean, pd.Series.std],
                                  values=['qlen', 'wait'], reindex_range=reindex_range)
    source_pivot = generic_pivot_time(pd.get_dummies(df.set_index('entry')['source']), time_axes, aggfunc='sum',
                                      reindex_range=reindex_range)
    resource_pivot = generic_pivot_time(pd.get_dummies(df.set_index('exit')['resource']), time_axes, aggfunc='sum',
                                        reindex_range=reindex_range)
    total_pivot = generic_pivot_time(pd.get_dummies(r.total_ts['event']), time_axes, aggfunc='sum',
                                     reindex_range=reindex_range)

    f, (ax1, ax2, ax3) = plt.subplots(3, 1, sharex='all', figsize=(12, 6), constrained_layout=True)
    en_pivot[('mean', 'qlen')].plot(marker='.', ax=ax1)
    ax1.fill_between(range(len(en_pivot)), en_pivot[('mean', 'qlen')] - en_pivot[('std', 'qlen')],
                     en_pivot[('mean', 'qlen')] + en_pivot[('std', 'qlen')], alpha=.1)
    ax1.axhline(baseline_qlen_mean, color='red', alpha=.2)
    en_pivot.loc[:, ('mean', 'wait')] = en_pivot[('mean', 'wait')].dt.total_seconds()
    en_pivot.loc[:, ('std', 'wait')] = en_pivot[('std', 'wait')].dt.total_seconds()
    en_pivot[('mean', 'wait')].plot(marker='.', ax=ax2)
    ax2.fill_between(range(len(en_pivot)), en_pivot[('mean', 'wait')] - en_pivot[('std', 'wait')],
                     en_pivot[('mean', 'wait')] + en_pivot[('std', 'wait')], alpha=.1)
    ax2.axhline(baseline_wait_mean, color='red', alpha=.2)
    ax1.legend(['queue length on arrival'])
    ax2.legend(['wait on arrival'])

    source_pivot.plot.bar(stacked=True, ax=ax3)
    neg = -resource_pivot
    neg.plot.bar(stacked=True, ax=ax3, color='red')
    plt.xticks(**xticks_args)
    plt.xlabel('')
    plt.show()


def pivot_along_week_hour(q: Queue):
    return pivot_along_time(q, time_axes=('dayofweek', 'hour'), reindex_range=(range(7), range(24)),
                            xticks_args={'ticks': [24 * i for i in range(7)],
                                         'labels': ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday',
                                                    'Sunday']})


def pivot_along_day(q: Queue):
    return pivot_along_time(q, time_axes=('hour',), reindex_range=(range(24),),
                            xticks_args={'ticks': range(24),
                                         'labels': [datetime.time(hour=i) for i in range(24)]})


def pivot_along_week(q: Queue):
    return pivot_along_time(q, time_axes=('dayofweek',), reindex_range=(range(7),),
                            xticks_args={'ticks': range(7),
                                         'labels': ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday',
                                                    'Sunday']})


def pivot_along_month(q: Queue):
    return pivot_along_time(q, time_axes=('day',), reindex_range=(range(32),),
                            xticks_args={'ticks': range(1, 32),
                                         'labels': [f'Day {i}' for i in range(1, 32)]})
