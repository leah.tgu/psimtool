from collections import defaultdict
import pandas as pd
import numpy as np
import datetime

import util


class Queue:

    def __init__(self, name):
        self.name = name
        self.entries = []
        self.exits = []
        self.sources = []
        self.resources = []
        self.case_ids = []
        self.attributes = defaultdict(list)
        self.attributes.update({'case_id': [], 'source': [], 'duration': [], 'resource': []})
        self.df = None
        self.replay = None
        self.occurrences = 0

    def is_empty(self):
        return self.occurrences == 0

    def enter(self, time: datetime.datetime, case_id, source, duration, **kwargs):
        self.entries.append(time)
        if case_id is None:
            case_id = self.occurrences
        self._append_attributes({'case_id': case_id, 'source': source, 'duration': duration})
        self._append_attributes(kwargs)
        self.occurrences += 1

    def exit(self, time: datetime.datetime, resource, **kwargs):
        self.exits.append(time)
        self._append_attributes({'resource': resource})
        self._append_attributes(kwargs)

    def _append_attributes(self, kwargs):
        for k, v in kwargs.items():
            self.attributes[k].append(v)

    def get_observation_start(self):
        df = self.get_data()
        return min(min(df.entry), min(df.exit))

    def get_observation_end(self):
        df = self.get_data()
        return max(max(df.entry), max(df.exit))

    def get_observation_period(self):
        return self.get_observation_end() - self.get_observation_start()

    def get_data(self) -> pd.DataFrame:
        if self.df is None:
            dic = {'entry': pd.DatetimeIndex(pd.to_datetime(self.entries, utc=True)),
                   'exit': pd.DatetimeIndex(pd.to_datetime(self.exits, utc=True))}
            dic.update(self.attributes)
            self.df = pd.DataFrame(data=dic)
            self.df['wait'] = (self.df.exit - self.df.entry)
            self.df.source = self.df.source.astype('category', copy=False)
            self.df.resource = self.df.resource.astype('category', copy=False)
        return self.df

    def _replay_queue(self):
        df = self.get_data()
        duration = df['duration']
        en = df.entry
        ex = df.exit

        en_sort = np.argsort(en)
        ex_sort = np.argsort(ex)

        n = len(df)
        idx, idy = 0, 0
        queue_length = 0

        FIFO, LIFO, SPT, Q_gt1 = 0, 0, 0, 0
        current_queue = []

        cont_pt_positions = []

        queue_length_ts = []
        type_ts = []
        ts_index = []
        ts_length = 0

        qlen_list, qlex_list = [], []
        deq_index_list = []
        overtaking_list = []
        ql_gt1_list = []

        while idx < n:
            while idx < n and en.iloc[en_sort[idx]] <= ex.iloc[ex_sort[idy]]:
                up = en_sort[idx]
                current_queue.append(up)

                qlen_list.append(queue_length)

                queue_length += 1
                queue_length_ts.append(queue_length)
                type_ts.append('up')
                time_up = en.iloc[en_sort[idx]]
                ts_index.append(time_up)
                ts_length += 1

                idx += 1
            while idy < n and (idx == n or en.iloc[en_sort[idx]] > ex.iloc[ex_sort[idy]]):
                down = ex_sort[idy]

                index = current_queue.index(down)
                if queue_length > 1:
                    spt = duration.iloc[current_queue].min()
                    pt = duration.iloc[down]
                    if pt == spt:
                        SPT += 1

                    processing_time_rank = duration.iloc[current_queue].rank(method='min').loc[down]
                    cont_pt_positions.append((queue_length - processing_time_rank - 1) / float(queue_length - 1))

                    entry_time_rank = en.iloc[current_queue].rank(method='min').loc[down]
                    overtaking_list.append(entry_time_rank - 1)

                    if index == 0:
                        FIFO += 1
                    elif index == queue_length - 1:
                        LIFO += 1

                    Q_gt1 += 1

                    ql_gt1_list.append(True)
                    current_queue.remove(down)
                else:
                    ql_gt1_list.append(False)
                    overtaking_list.append(np.nan)
                    current_queue.clear()

                qlex_list.append(queue_length)
                deq_index_list.append(index)

                queue_length -= 1
                queue_length_ts.append(queue_length)
                type_ts.append('down')
                time_down = ex.iloc[ex_sort[idy]]
                ts_index.append(time_down)
                ts_length += 1

                idy += 1

        en_sort_rev = np.empty_like(en_sort)
        ex_sort_rev = np.empty_like(ex_sort)
        en_sort_rev[en_sort] = np.arange(en_sort.size)
        ex_sort_rev[ex_sort] = np.arange(en_sort.size)

        df['qlen'] = np.array(qlen_list)[en_sort_rev]
        df['qlex'] = np.array(qlex_list)[ex_sort_rev]
        df['dequeue index'] = np.array(deq_index_list)[ex_sort_rev]
        df['ql_gt1'] = np.array(ql_gt1_list)[ex_sort_rev]
        df['overtaken'] = np.array(overtaking_list)[ex_sort_rev]
        df['continuous overtaken'] = df['overtaken'] / (df['qlex'] - 1)
        df['continuous dequeue index'] = df['dequeue index'] / (df['qlex'] - 1)

        total_ts = pd.DataFrame(index=pd.DatetimeIndex(pd.to_datetime(ts_index, utc=True)),
                                data={'queue length': queue_length_ts, 'event': type_ts})
        total_ts.event = total_ts.event.astype('category', copy=False)

        stats = {'queueing events': ts_length,
                 'interesting exits': Q_gt1,
                 'continuous lifo': df['continuous dequeue index'][df.ql_gt1].mean() if Q_gt1 > 0 else np.nan,
                 'dequeue index entropy': util.avg_grouped_entropy(df[df.ql_gt1], 'dequeue index', 'qlex',
                                                                   filter_by_size=True) if Q_gt1 > 0 else np.nan,
                 'dequeue index pseudo cont entropy': util.pseudo_continuous_entropy(
                     df['continuous dequeue index'][df.ql_gt1]) if Q_gt1 > 0 else np.nan,
                 'avg continuous overtaken': df['continuous overtaken'][df.ql_gt1].mean() if Q_gt1 > 0 else np.nan,
                 'spt': SPT / Q_gt1 if Q_gt1 > 0 else np.nan,
                 'spt cont mean': np.mean(cont_pt_positions) if Q_gt1 > 0 else np.nan,
                 }

        return QueueReplay(total_ts, stats)

    def get_replay(self):
        if self.replay is None:
            self.replay = self._replay_queue()
        return self.replay

    def reset(self):
        self.replay = None
        self._replay_queue()

    def get_interarrivals(self) -> pd.DataFrame:
        df = self.get_data()
        en_sort = df.entry.sort_values().reset_index(drop=True)
        ex_sort = df.exit.sort_values().reset_index(drop=True)
        res = pd.concat([en_sort, en_sort.diff().shift(-1, fill_value=np.nan).reset_index(drop=True), ex_sort,
                         ex_sort.diff().shift(-1, fill_value=np.nan).reset_index(drop=True)], axis=1).iloc[:-1]
        res.columns = ['entry', 'entry_ia', 'exit', 'exit_ia']
        return res


class QueueReplay:

    def __init__(self, total_ts, stats):
        self.stats = stats
        self.total_ts = total_ts
