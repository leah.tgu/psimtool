from collections import defaultdict, Counter

from pm4py.util import xes_constants


def lifecycle_census(log):
    lifecycles = defaultdict(list)

    for trace in log:

        transitions = defaultdict(list)
        for event in trace:
            act = event[xes_constants.DEFAULT_NAME_KEY]
            transitions[act].append(event[xes_constants.DEFAULT_TRANSITION_KEY])

        for k, v in transitions.items():
            lifecycles[k].append(tuple(v))

    for k, v in lifecycles.items():
        print(k)
        ok_c = 0
        all_c = 0
        print(f'\tcount\t\tvalid\tlifecycle sequence')
        for t, c in sorted(Counter(v).items(), key=lambda tup: tup[1], reverse=True):
            aut = LifecycleAutomaton()
            ok = all(aut.transition(lt) for lt in t) and aut.is_final_state()
            if ok:
                ok_c += c
            all_c += c
            print(f'\t{c}({100 * c / len(v):.2f}%)\t{ok}\t{t}')
        print(f'\ttotal valid lifecycles: {ok_c}/{all_c}({100 * ok_c / all_c:.2f}%)')


class LifecycleAutomaton:
    xes_states = {'start': [('schedule', 'ready'), ('autoskip', 'obsolete')],
                  'ready': [('assign', 'assigned')],
                  'assigned': [('reassign', 'assign'), ('start', 'inprogress')],
                  'inprogress': [('suspend', 'suspended'), ('complete', 'completed')],
                  'suspended': [('resume', 'inprogress')],
                  'open': [('pi_abort', 'aborted')],
                  'running': [('ate_abort', 'aborted')],
                  'notrunning': [('withdraw', 'exited'), ('manualskip', 'obsolete')]}

    xes_state_groups = {'ready': 'notrunning',
                        'assigned': 'notrunning',
                        'inprogress': 'running',
                        'suspended': 'running',
                        'notrunning': 'open',
                        'running': 'open',
                        'exited': 'closed',
                        'obsolete': 'closed',
                        'aborted': 'closed',
                        'completed': 'closed',
                        }

    custom_states = {'start': [('schedule', 'ready'), ('autoskip', 'obsolete')],
                     'ready': [('start', 'inprogress')],
                     'inprogress': [('suspend', 'suspended'), ('complete', 'completed')],
                     'suspended': [('resume', 'inprogress'), ('suspend', 'suspended')],
                     'open': [('pi_abort', 'aborted')],
                     'running': [('ate_abort', 'aborted')],
                     'notrunning': [('withdraw', 'exited'), ('manualskip', 'obsolete')]}

    simple_states = {'start': [('schedule', 'ready'), ('start', 'inprogress'), ('complete', 'completed')],
                     'ready': [('start', 'inprogress')],
                     'inprogress': [('complete', 'completed')]}

    def __init__(self, transition_system=None) -> None:
        self.state = 'start'
        self.transition_system = LifecycleAutomaton.simple_states if transition_system is None else transition_system

    def _transition(self, state, name):
        for transition in self.transition_system.get(state, ()):
            if transition[0] == name:
                return transition[1]
        if state in self.xes_state_groups:
            return self._transition(self.xes_state_groups[state], name)
        else:
            return None

    def transition(self, name):
        if self.is_final_state():
            self.reset()  # TODO autoloop here??
        next_state = self._transition(self.state, name)
        if next_state is None:
            pass
            # print(f'invalid transition {name} from {self.state}.')
        else:
            self.state = next_state
        return next_state

    def is_initial_state(self):
        return self.state == 'start'

    def is_final_state(self):
        return self.state in ['aborted', 'completed', 'obsolete', 'exited']

    def reset(self):
        self.state = 'start'
