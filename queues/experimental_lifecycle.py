from collections import defaultdict, Counter

from pm4py.util import xes_constants
import pandas as pd
import numpy as np

import util
from queue_model import QueueReplay
from queues.lifecycles import LifecycleAutomaton


class Connections:

    def __init__(self, ingoing=None, outgoing=None) -> None:
        self.ingoing = ingoing if ingoing is not None else []
        self.outgoing = outgoing if outgoing is not None else []


class QueueRelations:

    def __init__(self) -> None:
        self.items = []
        self.internal = defaultdict(Connections)

    def populate_relations(self, items, edges):
        self.items = items
        for s, t in edges:
            if s in items and t in items:
                self.internal[s].outgoing.append(t)
                self.internal[t].ingoing.append(s)

    def __getitem__(self, item):
        return self.internal[item]


class SimpleQueue:

    def __init__(self) -> None:
        self.internal = []
        self.length = 0

    def append(self, item):
        self.internal.append(item)
        self.length += 1

    def position(self, item):
        return self.internal.index(item)

    def remove(self, item):
        self.internal.remove(item)
        self.length -= 1

    def size(self):
        return self.length

    def clear(self):
        self.internal.clear()
        self.length = 0


class ProcessingReplay:

    def __init__(self, total_ts, stats) -> None:
        self.total_ts = total_ts
        self.stats = stats


class LifecycleQueue:

    def __init__(self, name, df) -> None:
        self.name = name
        self.df = df
        self.replay = None
        self.processing_replay = None

    def get_data(self):
        return self.df

    def get_observation_start(self):
        return min(self.df.entry)

    def get_observation_end(self):
        return max(self.df.exit)

    def get_observation_period(self):
        return self.get_observation_end() - self.get_observation_start()

    def get_predecessor_waits(self):
        df = self.df
        predecessor_waits = defaultdict(list)
        for i in range(len(df)):
            for pred, time in zip(df.predecessors.iloc[i], df.predecessor_times.iloc[i]):
                predecessor_waits[pred].append(df.entry.iloc[i] - time)
        return predecessor_waits

    def _replay_queue(self):
        df = self.df
        n = len(df)
        processing_time = df.processing_time

        current_queue = SimpleQueue()

        en = df.entry
        en_argsort = np.argsort(en)
        en_index = df.index[en_argsort]
        st = df.start
        st_argsort = np.argsort(st)
        st_index = df.index[st_argsort]

        en_id, st_id = 0, 0
        deq, spt = [], []
        qlen, qlex = [], []
        ql_ts_index, ql_ts, qe_ts = [], [], []

        while en_id < n:
            while en_id < n and en.loc[en_index[en_id]] <= st.loc[st_index[st_id]]:
                q_entry = en_index[en_id]
                qlen.append(current_queue.size())
                current_queue.append(q_entry)
                ql_ts.append(current_queue.size())
                qe_ts.append('up')
                ql_ts_index.append(en.loc[q_entry])
                en_id += 1
            while st_id < n and (en_id == n or en.loc[en_index[en_id]] > st.loc[st_index[st_id]]):
                q_exit = st_index[st_id]
                dequeue_index = current_queue.position(q_exit)
                deq.append(dequeue_index)
                size = current_queue.size()
                qlex.append(size)
                if size > 1:
                    processing_time_rank = processing_time.loc[current_queue.internal].rank(method='min').loc[q_exit]
                    spt.append(size - processing_time_rank)
                    current_queue.remove(q_exit)
                else:
                    spt.append(0)
                    current_queue.clear()

                ql_ts.append(current_queue.size())
                qe_ts.append('down')
                ql_ts_index.append(st.loc[q_exit])
                st_id += 1

        en_rev_index = df.index[util.reverse_sort(en_argsort)]
        st_rev_index = df.index[util.reverse_sort(st_argsort)]

        df.loc[en_rev_index, 'qlen'] = qlen
        df.loc[st_rev_index, 'qlex'] = qlex
        df.loc[st_rev_index, 'deq'] = deq
        df.loc[st_rev_index, 'spt'] = spt

        df.loc[:, 'ql_gt1'] = df.qlex > 1
        df.loc[df.ql_gt1, 'cont_deq'] = df.deq[df.ql_gt1] / (df.qlex[df.ql_gt1] - 1)
        df.loc[df.ql_gt1, 'cont_spt'] = df.spt[df.ql_gt1] / (df.qlex[df.ql_gt1] - 1)

        total_ts = pd.DataFrame(index=pd.DatetimeIndex(ql_ts_index),
                                data={'queue length': ql_ts, 'event': qe_ts})
        total_ts.event = total_ts.event.astype('category', copy=False)

        ql_gt1_count = df.ql_gt1.sum()
        stats = {
            'queue events': len(total_ts),
            'interesting queue exits': ql_gt1_count,
            'continuous lifo': df.cont_deq.mean() if ql_gt1_count > 0 else np.nan,
            'lifo determinism measure_durations': util.pseudo_continuous_entropy(
                df.cont_deq) if ql_gt1_count > 0 else np.nan,
            'continuous spt': df.cont_spt.mean() if ql_gt1_count > 0 else np.nan,
            'spt determinism measure_durations': util.pseudo_continuous_entropy(
                df.cont_spt) if ql_gt1_count > 0 else np.nan,
        }

        return QueueReplay(total_ts, stats)

    def _replay_processing(self):
        df = self.df
        n = len(df)

        resource = df.exit_resource

        currently_processing = SimpleQueue()

        active_resources = Counter()

        st = df.start
        st_argsort = np.argsort(st)
        st_index = df.index[st_argsort]
        ex = df.exit
        ex_argsort = np.argsort(ex)
        ex_index = df.index[ex_argsort]

        st_id, ex_id = 0, 0
        completion_indices, spt = [], []
        ccst, ccex = [], []
        is_concurrent_processing = []
        ts_index, concurrent_count_ts, active_resources_ts, processing_event = [], [], [], []

        while st_id < n:
            while st_id < n and st.loc[st_index[st_id]] <= ex.loc[ex_index[ex_id]]:
                item = st_index[st_id]
                res = resource.loc[item]
                active_resources[res] += 1
                ccst.append(currently_processing.size())
                currently_processing.append(item)
                concurrent_count_ts.append(currently_processing.size())
                active_resources_ts.append(active_resources.copy())
                processing_event.append('start')
                ts_index.append(st.loc[item])
                st_id += 1
            while ex_id < n and (st_id == n or st.loc[st_index[st_id]] > ex.loc[ex_index[ex_id]]):
                item = ex_index[ex_id]

                res = resource.loc[item]
                active_resources[res] -= 1
                if active_resources[res] <= 0:
                    del active_resources[res]

                active_resources_ts.append(active_resources.copy())
                completion_index = currently_processing.position(item)
                completion_indices.append(completion_index)
                size = currently_processing.size()
                ccex.append(size)
                is_concurrent_processing.append(size > 1)
                currently_processing.remove(item)

                concurrent_count_ts.append(currently_processing.size())
                processing_event.append('end')
                ts_index.append(ex.loc[item])
                ex_id += 1

        st_rev_index = df.index[util.reverse_sort(st_argsort)]
        ex_rev_index = df.index[util.reverse_sort(ex_argsort)]

        df.loc[st_rev_index, 'ccst'] = ccst
        df.loc[ex_rev_index, 'ccex'] = ccex
        df.loc[ex_rev_index, 'completion'] = completion_indices

        df.loc[:, 'cc_gt1'] = df.ccex > 1
        df.loc[df.cc_gt1, 'cont_comp'] = df.completion[df.cc_gt1] / (df.ccex[df.cc_gt1] - 1)

        total_ts = pd.DataFrame(index=pd.DatetimeIndex(ts_index),
                                data={'concurrent_count': concurrent_count_ts, 'active_resources': active_resources_ts,
                                      'event': processing_event})
        total_ts.event = total_ts.event.astype('category', copy=False)

        cc_gt1_count = df.cc_gt1.sum()
        stats = {
            'processing events': len(total_ts),
            'concurrency occurred': cc_gt1_count,
            'continuous completion order': df.cont_comp.mean() if cc_gt1_count > 0 else np.nan,
            'completion order determinism measure_durations': util.pseudo_continuous_entropy(
                df.cont_comp) if cc_gt1_count > 0 else np.nan
        }

        return ProcessingReplay(total_ts, stats)

    def get_replay(self):
        if self.replay is None:
            self.replay = self._replay_queue()
        return self.replay

    def get_processing_replay(self):
        if self.processing_replay is None:
            self.processing_replay = self._replay_processing()
        return self.processing_replay

    def get_interarrivals(self) -> pd.DataFrame:
        df = self.df
        en_sort = df.entry.sort_values().reset_index(drop=True)
        st_sort = df.start.sort_values().reset_index(drop=True)
        ex_sort = df.exit.sort_values().reset_index(drop=True)
        res = pd.concat([en_sort, en_sort.diff().shift(-1, fill_value=np.nan).reset_index(drop=True),
                         st_sort, st_sort.diff().shift(-1, fill_value=np.nan).reset_index(drop=True),
                         ex_sort, ex_sort.diff().shift(-1, fill_value=np.nan).reset_index(drop=True)],
                        axis=1).iloc[:-1]
        res.columns = ['entry', 'entry_ia', 'start', 'start_ia', 'exit', 'exit_ia']
        return res


class LifecycleQueueBuilder:

    def __init__(self, name) -> None:
        self.name = name
        self.rows = []
        self.lifecycle = LifecycleAutomaton()
        self._reset_open_item()

    def enter(self, time, resource, event_attributes):
        self.open_item['entry'] = time
        self.open_item['entry_resource'] = resource
        self.open_item.update(event_attributes)

    def start(self, time, resource):
        self.open_item['start'] = time
        self.open_item['start_resource'] = resource

    def abort(self, time, resource):
        self.open_item['exit'] = time
        self.open_item['exit_resource'] = resource
        self.open_item['successful'] = False

    def exit(self, time, resource):
        self.open_item['exit'] = time
        self.open_item['exit_resource'] = resource
        self.open_item['successful'] = True

    def add_pre(self, time, predecessor):
        self.open_item['predecessor_times'].append(time)
        self.open_item['predecessors'].append(predecessor)

    def _reset_open_item(self):
        self.open_item = {'case_id': None, 'entry': None, 'entry_resource': None, 'start': None, 'start_resource': None,
                          'exit': None,
                          'exit_resource': None, 'successful': None, 'predecessors': [], 'predecessor_times': [],
                          'lifecycles': []}
        self.lifecycle.reset()

    def _save_open_item(self):
        if len(self.open_item['predecessors']) == 0:
            self.open_item['predecessor_times'] = [self.open_item['entry']]
            self.open_item['predecessors'] = ['start']

        def tupler(d):
            return {k: tuple(v) if type(v) == list else v for k, v in d.items()}

        row = pd.Series(tupler(self.open_item))
        self._reset_open_item()
        self.rows.append(row)

    @staticmethod
    def _extended_columns(df):
        return df.assign(wait=df['start'] - df['entry'], processing_time=df['exit'] - df['start'],
                         interruptions=df['lifecycles'].apply(lambda tup: sum(1 for t in tup if t == 'suspend')))

    def build_queue(self):
        df = pd.DataFrame(self.rows)
        df = self._extended_columns(df)
        return LifecycleQueue(self.name, df.loc[df.notna().all(axis=1)].reset_index(drop=True))

    def begin_trace(self):
        self._reset_open_item()

    def end_trace(self):
        self._reset_open_item()

    def apply_predecessor(self, time, predecessor):
        self.add_pre(time, predecessor)

    def apply_event(self, event):
        time = event[xes_constants.DEFAULT_TIMESTAMP_KEY]
        # TODO NOONO timezones
        time = time.replace(tzinfo=None)
        lifecycle_transition = event[xes_constants.DEFAULT_TRANSITION_KEY]
        resource = event.get(xes_constants.DEFAULT_RESOURCE_KEY, 'n/a')

        self.open_item['lifecycles'].append(lifecycle_transition)

        next_state = self.lifecycle.transition(lifecycle_transition)

        if next_state is None:
            pass  # TODO ignore or handle
            # self._reset_open_item()
        elif next_state == 'ready':
            event_attributes = {'event_' + k: v for k, v in event.items() if
                                k not in [xes_constants.DEFAULT_NAME_KEY, xes_constants.DEFAULT_TIMESTAMP_KEY,
                                          xes_constants.DEFAULT_RESOURCE_KEY, xes_constants.DEFAULT_TRANSITION_KEY]}
            self.enter(time, resource, event_attributes)
        elif next_state == 'inprogress':
            self.start(time, resource)
        elif next_state in ['obsolete', 'aborted', 'exited']:
            self.abort(time, resource)
        elif next_state == 'completed':
            self.exit(time, resource)

    def apply_case_info(self, case_id, case_duration, case_attributes):
        self.open_item['case_id'] = case_id
        self.open_item['case_duration'] = case_duration
        self.open_item.update({'case_' + k: v for k, v in case_attributes.items()})

    def has_exited(self):
        return self.lifecycle.is_final_state()

    def close_case(self):
        end = self.open_item['exit']
        self._save_open_item()
        return end


class LifecycleQueueAdapter:

    @staticmethod
    def handle_trace(trace, builders, queue_relations):
        for builder in builders.values():
            builder.begin_trace()

        case_id = trace.attributes[xes_constants.DEFAULT_TRACEID_KEY]
        start = trace[0][xes_constants.DEFAULT_TIMESTAMP_KEY]
        end = trace[-1][xes_constants.DEFAULT_TIMESTAMP_KEY]
        duration = end - start
        case_attributes = {k: v for k, v in trace.attributes.items() if k != xes_constants.DEFAULT_TRACEID_KEY}

        dic = {}

        for event in trace:
            activity = event[xes_constants.DEFAULT_NAME_KEY]

            if activity in builders:
                builder = builders[activity]
                builder.apply_event(event)
                if builder.has_exited():
                    builder.apply_case_info(case_id, duration, case_attributes)
                    for predecessor in queue_relations[activity].ingoing:
                        if predecessor in dic:
                            finish = dic.pop(
                                predecessor)  # TODO only single successor possible, concurrency under attack
                            builder.apply_predecessor(finish, predecessor)

                    dic[activity] = builder.close_case()

        for builder in builders.values():
            builder.end_trace()

    @staticmethod
    def handle_log(log, queue_relations):
        builders = {a: LifecycleQueueBuilder(a) for a in queue_relations.items}
        for trace in log:
            LifecycleQueueAdapter.handle_trace(trace, builders, queue_relations)
        return QueueSystem({a: builder.build_queue() for a, builder in builders.items()}, queue_relations)


class QueueSystem:

    def __init__(self, queues, relations) -> None:
        self.queues = queues
        self.relations = relations

    def __getitem__(self, item):
        return self.queues.get(item)
