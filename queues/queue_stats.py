from queue_model import Queue
import numpy as np
import scipy
import util
from util import burstiness


def classify_qr(q: Queue):
    qr = q.get_replay()
    tags = []
    is_random = qr.stats['dequeue index entropy'] > .7 or qr.stats['dequeue index pseudo cont entropy'] > .7
    if qr.stats['continuous lifo'] < .3 and not is_random:
        tags.append('fifo like')
    elif qr.stats['continuous lifo'] > .7 and not is_random:
        tags.append('lifo like')
    elif is_random:
        tags.append('random like')
    if np.mean(qr.total_ts['queue length']) > 100 and np.percentile(qr.total_ts['queue length'], 50) > 100:
        tags.append('high capacity')
    if np.std(qr.total_ts['queue length']) > np.mean(qr.total_ts['queue length']):
        tags.append('high queue length variation')
    return tags


def classify_ias(q: Queue):
    ias = q.get_interarrivals()
    obs_period = q.get_observation_period()
    obs_start = q.get_observation_start()
    normed_exit_count = (ias.exit - obs_start) / obs_period
    normed_entry_count = (ias.entry - obs_start) / obs_period
    df = q.get_data()
    normed_shifted_entry_count = (ias.entry + df.wait.mean() - obs_start) / obs_period
    entry_int = scipy.integrate.trapz(np.linspace(0, 1, len(ias)), normed_entry_count)
    shifted_entry_int = scipy.integrate.trapz(np.linspace(0, 1, len(ias)), normed_shifted_entry_count)
    exit_int = scipy.integrate.trapz(np.linspace(0, 1, len(ias)), normed_exit_count)
    batching_maybe = abs(shifted_entry_int - exit_int) > 0.01

    test_1 = scipy.stats.anderson_ksamp([ias.entry_ia, ias.exit_ia]).significance_level <= 0.001
    test_2 = scipy.stats.ks_2samp(ias.entry_ia, ias.exit_ia).pvalue <= 0.001
    ias_differ = test_1 and test_2

    bursty_entry = burstiness(ias.entry_ia) > .5
    bursty_exit = burstiness(ias.exit_ia) > .5

    tags = []
    if batching_maybe:
        tags.append('possible batching')
    if ias_differ:
        tags.append('statistical difference between entry and exit ias')
    if bursty_entry:
        tags.append('entry ias are bursty')
    if bursty_exit:
        tags.append('exit ias are bursty')

    return tags


def classify_df(q: Queue):
    df = q.get_data()
    len_wait_corr = np.corrcoef(df.qlen, df.wait.dt.total_seconds())[0, 1]
    is_len_wait_corr = len_wait_corr > .5
    wat = len_wait_corr < -.5

    is_bad_omen = np.corrcoef(df.wait.dt.total_seconds(), df.duration.dt.total_seconds())[0, 1] > .5

    input_dependence, biggest_diffs = util.dist_change(df, 'wait', 'source')

    entry_hours = business_hours(df.set_index('entry'))
    exit_hours = business_hours(df.set_index('exit'))
    resources = resource_dist(df)

    ins, outs = util.resample_in_n_out(df, '1M')

    zins = scipy.stats.zscore(ins.sum(axis=1))
    zouts = scipy.stats.zscore(outs.sum(axis=1))
    in_variation = np.count_nonzero(np.abs(zins) >= 3) > 1
    out_variation = np.count_nonzero(np.abs(zouts) >= 3) > 1

    tags = []
    if is_len_wait_corr:
        tags.append('wait correlated with queue length')
    elif wat:
        tags.append('wait negatively correlated with queue length')
    else:
        tags.append('wait not correlated with queue length')
    if is_bad_omen:
        tags.append('case duration is correlated with wait')
    if input_dependence:
        tags.append(f'statistical difference between queue sources {biggest_diffs[0][0], biggest_diffs[0][1]}')
    tags.append(entry_hours)
    tags.append(exit_hours)
    tags.append(resources)
    if in_variation:
        tags.append('number of queue entries fluctuate')
    if out_variation:
        tags.append('number of queue exits fluctuate')

    return tags


def business_hours(df):
    length = len(df)

    h_sizes = df.groupby(df.index.hour).size()
    hours = h_sizes >= .5 * 1 / 24 * length

    dw_sizes = df.groupby(df.index.dayofweek).size()
    daysofweek = dw_sizes >= .5 * 1 / 7 * length

    dm_sizes = df.groupby(df.index.day).size()
    daysofmonth = dm_sizes >= .5 * 1 / 30 * length

    return hours[hours].index.values, daysofweek[daysofweek].index.values, daysofmonth[daysofmonth].index.values


def resource_dist(df):
    length = len(df)
    vc = df.resource.value_counts()
    return vc[vc >= .1 * 1 / len(df.resource.cat.categories) * length].index.values


