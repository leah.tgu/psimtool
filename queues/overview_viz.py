from queue_model import *
import pivots
import viz


def vis_queue(q: Queue):
    plt.rcParams["figure.figsize"] = (15, 9)

    qr = q.get_replay()
    df = q.get_data()
    ias = q.get_interarrivals()

    print(qr.stats)

    qr.total_ts.plot()
    plt.show()

    viz.vis_in_n_outs(q, freq='M')
    plt.show()

    pivots.pivot_along_day(q)
    pivots.pivot_along_week(q)
    pivots.pivot_along_month(q)

    viz.my_hist(ias.entry_ia)
    viz.my_hist(ias.exit_ia)
    plt.xlabel('inter event time')
    plt.title('Inter event times of queue entries/exits')
    plt.legend(['entries', 'exits'])
    plt.show()

    viz.vis_iat_over_time(q)
    plt.show()

    viz.my_hist(df.wait)
    plt.title('histogram of queue wait times')
    plt.show()

    viz.vis_wait_over_time(q)
    plt.show()

    viz.vis_dequeue_index(q)
    plt.show()

