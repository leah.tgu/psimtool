import itertools

import pm4py
import queue_model
from typing import Dict
from pm4py.util import xes_constants
from pm4py.algo.conformance.alignments import algorithm as align
from pm4py.algo.discovery.inductive import algorithm as inductive_miner

import util


def derive_from_dfg(log: pm4py.objects.log, dfg: pm4py.objects.dfg) -> Dict[
    str, queue_model.Queue]:
    activities = util.get_dfg_activities(dfg)

    queue_dict = {a: queue_model.Queue(a) for a in activities}

    inputs = {}

    for (v, w) in dfg.keys():
        if w not in inputs:
            inputs[w] = []
        inputs[w].append(v)

    last_times = {}
    for trace_index, trace in enumerate(log):
        case_id = trace.attributes[xes_constants.DEFAULT_TRACEID_KEY]

        start = trace[0][xes_constants.DEFAULT_TIMESTAMP_KEY]
        end = trace[-1][xes_constants.DEFAULT_TIMESTAMP_KEY]
        duration = end - start

        for event, following_event in itertools.zip_longest(trace, trace[1:]):
            activity = event[xes_constants.DEFAULT_NAME_KEY]
            time = event[xes_constants.DEFAULT_TIMESTAMP_KEY]

            if activity in inputs:
                for i in inputs[activity]:
                    if i in last_times:
                        queue_dict[activity].enter(last_times[i], case_id=case_id, source=i, duration=duration)
                        if following_event is not None:
                            following_activity = following_event[xes_constants.DEFAULT_NAME_KEY]
                        else:
                            following_activity = 'exit'
                        queue_dict[activity].exit(time,
                                                  resource=event.get(xes_constants.DEFAULT_RESOURCE_KEY, 'default'),
                                                  target=following_activity)

            last_times[activity] = time

        last_times.clear()

    return queue_dict


def derive_using_alignments(log: pm4py.objects.log, model=None):
    if model is None:
        model = inductive_miner.apply(log)
    net, initial, final = model
    alignments = align.apply_log_multiprocessing(log, net, initial, final, parameters={"ret_tuple_as_trans_desc": True})

    trans = {t.label: t for t in net.transitions}

    queues = {p: queue_model.Queue(str(p)) for p in net.places}

    def execute(t_name, time, resource, **kwargs):
        t = trans[t_name]
        if time is None:
            time = max(marking[a.source] for a in t.in_arcs)
        for a in t.out_arcs:
            if a.target not in final:
                marking[a.target] = time
                queues[a.target].enter(time, source=str(t), **kwargs)
        for a in t.in_arcs:
            del marking[a.source]
            if a.source not in initial:
                queues[a.source].exit(time, resource=resource, target=str(t))
        return time

    for alignment_dict, trace in zip(alignments, log):
        alignment = alignment_dict['alignment']
        case_id = trace.attributes[xes_constants.DEFAULT_TRACEID_KEY]
        start = trace[0][xes_constants.DEFAULT_TIMESTAMP_KEY]
        end = trace[-1][xes_constants.DEFAULT_TIMESTAMP_KEY]
        duration = end - start
        marking = {p: start for p in initial}

        i, j = 0, 0
        while i < len(alignment):
            if alignment[i][0][1] != '>>':  # not log move
                if alignment[i][0][0] == '>>':  # model move
                    name = 'silent'
                    time = None
                    resource = 'default'
                else:
                    e = trace[j]
                    name = e[xes_constants.DEFAULT_NAME_KEY]
                    time = e[xes_constants.DEFAULT_TIMESTAMP_KEY]
                    resource = e.get(xes_constants.DEFAULT_RESOURCE_KEY, 'default')
                    j += 1
                execute(alignment[i][0][1], time, resource, case_id=case_id, duration=duration)
            else:
                j += 1
            i += 1

    return {str(p): q for p, q in queues.items()}
