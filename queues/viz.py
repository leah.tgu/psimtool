from fitter import Fitter
from queue_model import Queue
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
import util
import pandas as pd
import scipy


def my_hist(series, logx=False, logy=False, normalized=False, draw_ecdf=False):
    if draw_ecdf:
        f, (ax1, ax2) = plt.subplots(1, 2, figsize=(15, 6))
        if type(series) == list:
            for s in series:
                mids, counts = util.bin_dynamically(s, normalized, False)
                sns.scatterplot(mids, counts, ax=ax1)
                sns.lineplot(mids, counts.cumsum(), ax=ax2)
        else:
            mids, counts = util.bin_dynamically(series, normalized, False)
            sns.scatterplot(mids, counts, ax=ax1)
            sns.lineplot(mids, counts.cumsum(), ax=ax2)
        ax1.set_title('histogram')
        ax2.set_title('empirical cdf')
        if logx:
            ax1.xaxis.scale('log')
            ax2.xaxis.scale('log')
        if logy:
            ax1.yaxis.scale('log')
            ax2.yaxis.scale('log')
        return ax1, ax2
    else:
        f, ax = plt.subplots(1, 1, figsize=(15, 6))
        if type(series) == list:
            for s in series:
                mids, counts = util.bin_dynamically(s, normalized, False)
                sns.scatterplot(mids, counts, ax=ax)
        else:
            mids, counts = util.bin_dynamically(series, normalized, False)
            sns.scatterplot(mids, counts, ax=ax)
        if logx:
            plt.xscale('log')
        if logy:
            plt.yscale('log')
        return ax


def my_hist(series, logx=False, logy=False, normalized=False, draw_ecdf=False):
    return sns.distplot(series, bins='auto', rug=True)


def my_ecdf(series):
    plt.plot(*util.ecdf(series))


def my_cont_ecdf(series):
    plt.plot(*util.binned_ecdf(series))


def format_date_axis(ax):
    ax.figure.autofmt_xdate()
    # ax.xaxis.set_major_locator()
    # ax.xaxis.set_major_formatter(mdates.AutoDateFormatter(mdates.AutoDateLocator()))
    # ax.xaxis.set_minor_locator(mdates.MonthLocator())


def vis_in_n_outs(q: Queue, freq='1w'):
    ins, outs = util.resample_in_n_out(q.get_data(), freq=freq)
    f, ax = plt.subplots(figsize=(12, 6))

    ins.plot.bar(linewidth=0.2, ax=ax, stacked=True)
    (-1 * outs).plot.bar(ax=ax, stacked=True)
    format_date_axis(ax)
    plt.xlabel('')
    plt.title(f'number of queue entries/exits per {freq}')


def vis_iat_over_time(q: Queue):
    df = q.get_data()
    wait_mean = df.wait.mean()
    obs_start = q.get_observation_start()
    obs_period = obs_period = max(df.exit.max(), df.entry.max() + wait_mean) - obs_start
    ias = q.get_interarrivals()
    normed_entry_count = (ias.entry - obs_start) / obs_period
    normed_shifted_entry_count = (ias.entry + wait_mean - obs_start) / obs_period
    normed_exit_count = (ias.exit - obs_start) / obs_period
    plt.plot(normed_entry_count, np.linspace(0, 1, len(ias)), label='entries')
    plt.plot(normed_shifted_entry_count, np.linspace(0, 1, len(ias)), label='entries shifted by avg wait')
    plt.plot(normed_exit_count, np.linspace(0, 1, len(ias)), label='exits')
    plt.plot([0, 1], [0, 1], label='constant')
    plt.legend()


def vis_dequeue_index(q: Queue):
    df = q.get_data()
    # deq_evs = df.ql_gt1.sum()
    my = df['continuous dequeue index'][df.ql_gt1]
    poss = df.qlex[df.ql_gt1].apply(lambda qlex: scipy.stats.randint(0, qlex).rvs() / (qlex - 1))
    # poss = payment_queue_df['qlex'][payment_queue_df.ql_gt1].apply(
    #   lambda qlex: (qlex - 1 - scipy.stats.poisson(1).rvs()) / (qlex - 1))
    ax1, ax2 = my_hist([my, poss], draw_ecdf=True)
    print(scipy.stats.ks_2samp(my, poss))
    ax1.legend(['observed', 'simulated random'])
    ax2.legend(['observed', 'simulated random'])
    ax1.set_xlabel('normalized dequeue index')
    ax2.set_xlabel('normalized dequeue index')
    plt.suptitle('histogram of the normalized dequeue index')


def vis_wait_over_time(q: Queue):
    df = q.get_data()
    if not df.empty:
        waits_ts = pd.Series(index=pd.DatetimeIndex(pd.to_datetime(df.entry, utc=True)), data=df.wait.values)
        waits_ts.sort_index().plot(marker='o', linestyle='none')
        plt.xlabel('time of entry')
        plt.ylabel('wait in queue')
        plt.title('wait in queue over time')


def dist_fitter(vals):
    f = Fitter(vals, distributions=['exponential', 'powerlaw'], verbose=False)
    f.fit()
    print(f.summary(plot=False))
    # pois = sm.Poisson(vals, np.ones_like(vals)).fit()
    # print(pois.summary())


def vis_wait_corr(q: Queue):
    df = q.get_data()
    ax = sns.swarmplot(x=df.qlen, y=df.wait.dt.total_seconds(),
                       hue=df.source)
    ax.set_title(f'qlen vs wait (corr: {df.qlen.corr(df.wait.dt.total_seconds())})')
    plt.show()


def vis_iat(q: Queue):
    ia = q.get_interarrivals()
    if not ia.empty:
        plt.figure(figsize=(15, 5))
        plt.subplot(121)
        sns.distplot(ia.entry_ia)
        plt.title('inter arrival of entries')
        plt.subplot(122)
        sns.distplot(ia.exit_ia)
        plt.title('inter arrival of exits')
        plt.show()


def vis_waits(q: Queue):
    df = q.get_data()
    if not df.wait.empty:
        plt.figure(figsize=(15, 5))
        plt.subplot(121)
        sns.distplot(df.wait)
        plt.title('wait distribution')
        plt.subplot(122)
        waits_ts = pd.Series(index=pd.DatetimeIndex(pd.to_datetime(df.exit, utc=True)), data=df.wait.values)
        waits_ts.sort_index(inplace=True)
        plt.plot(waits_ts)
        plt.title('wait over time')
        plt.gcf().autofmt_xdate()
        plt.show()


def fit_iat(q: Queue):
    print(f'fitting inter arrivals for {q.name}')
    df = q.get_interarrivals()
    if not df.empty:
        # plt.figure(figsize=(15,5))
        # plt.subplot(121)
        dist_fitter(df.entry.astype('timedelta64[s]').values)
        # plt.subplot(122)
        dist_fitter(df.exit.astype('timedelta64[s]').values)
        # plt.show()


def fit_waits(q: Queue):
    print('Replaying Queue', q.name)
    df = q.get_data()
    if not df.wait.empty:
        dist_fitter(df.wait.astype('timedelta64[s]').values)
        plt.show()
