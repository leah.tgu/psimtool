import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import scipy.stats
from pm4py.objects.log.log import EventLog, Trace


def general_log_filter(log, event_filter):
    nl = EventLog()

    for trace in log:

        nt = Trace()
        for attr in trace.attributes:
            nt.attributes[attr] = trace.attributes[attr]
        for event in trace:
            if event_filter(event):
                nt.append(event)

        if len(nt) > 0:
            nl.append(nt)
    return nl


def simplify_queue_level_ts(df):
    return df.loc[~df.index.duplicated(keep='last')]


def get_dfg_activities(dfg):
    result = set()
    for v, w in dfg.keys():
        result.add(v)
        result.add(w)
    return list(result)


def entropy(p):
    return -np.sum(p[p > 0] * np.log(p[p > 0]), axis=0)


def interpolate(low, high, value):
    return np.int_(np.minimum(value / high, 1) * (high - low) + low)


def log_bin_count(value):
    return np.int_(20 * (np.log1p(value) + 1))


def pseudo_continuous_entropy(series):
    series = series[series.notna()]  # TODO ooh watch out
    length = len(series)
    cuts = log_bin_count(length)
    probs = series.groupby(pd.cut(series, cuts)).count().values / length
    return scipy.stats.entropy(probs[probs > 0]) / np.log(cuts)


def avg_grouped_entropy(df, option_key, num_option_key, filter_by_size=False):
    if len(df) == 0:
        return np.nan

    df = df[[option_key, num_option_key]]

    def weighted_randomness(g):
        length = len(g)
        vals = (np.nan, np.nan)  # if group has size of less than 10% of possible values
        if length > 0 and (length > 0.1 * g.label or not filter_by_size):
            counts = g.groupby(option_key).count().values
            vals = scipy.stats.entropy(counts[counts > 0] / length) * length / np.log(length), length
        return pd.Series(vals, index=['group entropy', 'group size'])

    res = df.groupby(num_option_key).apply(weighted_randomness).sum()

    return res['group entropy'] / res['group size']


# TODO make better
def statistical_tests(df, dependent_var, independent_var=None, resample=None):
    if resample is not None:
        grouped = df.resample(resample)[dependent_var]
    elif independent_var is not None:
        grouped = df.groupby(independent_var)[dependent_var]
    else:
        return
    group_names = list(grouped.groups.keys())
    print('groups:', len(group_names))
    groups = [grouped.get_group(g) for g in group_names]
    if len(group_names) < 50:
        big_diff = pairwise_stat(group_names, groups,
                                 lambda g1, g2: abs(g1.mean() - g2.mean()), maximum=True)[
                   :5]
        print(big_diff)
    global_mean = df[dependent_var].mean()
    mean_diff = list(sorted(
        ((name, abs(g.mean() - global_mean)) for g, name in zip(groups, group_names)),
        key=lambda t: t[1], reverse=True))[:5]
    print(mean_diff)
    return scipy.stats.f_oneway(*groups), scipy.stats.kruskal(*groups)


def pairwise_stat(item_names, items, stat, maximum=True, key=lambda t: t[2]):
    return list(sorted(
        ((name1, name2) + stat(g1, g2) for i, (g1, name1) in enumerate(zip(items, item_names)) for (g2, name2) in
         zip(items[i + 1:], item_names[i + 1:])), key=key, reverse=maximum))


def linear_stat(item_names, items, stat, maximum=True, key=lambda t: t[2]):
    return list(sorted(((n1, n2) + stat(g1, g2) for n1, g1, n2, g2
                        in
                        zip(item_names[:-1], items[:-1], item_names[1:], items[1:])), key=key, reverse=maximum))


def dist_change(df, dependent_var, independent_var=None, resample=None, pairwise=False):
    if resample is not None:
        grouped = df.resample(resample)[dependent_var]
    elif independent_var is not None:
        grouped = df.groupby(independent_var)[dependent_var]
    else:
        return False, []
    group_names = list(grouped.groups.keys())

    if len(group_names) < 2:
        return False, []

    groups = [grouped.get_group(g) for g in group_names]
    if pd.api.types.is_categorical(df[dependent_var]):
        cats = df[dependent_var].cat.categories
        groups = [g.astype(pd.CategoricalDtype(cats, ordered=True)).value_counts() for g in groups]

    stat = lambda g1, g2: (
        scipy.stats.ks_2samp(g1.values, g2.values), scipy.stats.wasserstein_distance(g1.values, g2.values))
    key = lambda t: t[2].pvalue
    lin_diff = linear_stat(group_names, groups, stat, key=key, maximum=False)[:5]
    if pairwise:
        pair_diff = pairwise_stat(group_names, groups, stat, key=key, maximum=False)[:5]
        print(pair_diff)

    return scipy.stats.anderson_ksamp(groups).significance_level <= 0.001, lin_diff


def same_counts(df, independent_var=None, resample=None):
    if resample is not None:
        groups = df.resample(resample).size()
    elif independent_var is not None:
        groups = df.groupby(independent_var).size()
    else:
        return
    return scipy.stats.anderson_ksamp(groups.values)


def ecdf(series):
    sorted_series = series.sort_values()
    eia_counts = sorted_series.groupby(sorted_series).count()
    eia_counts = eia_counts / sum(eia_counts)
    return eia_counts.index.values, eia_counts.cumsum()


def get_dynamic_bins(series, logspaced=False):
    length = len(series)
    cuts = log_bin_count(length)
    if pd.api.types.is_timedelta64_dtype(series):
        series = series.dt.total_seconds()
    if pd.api.types.is_datetime64_any_dtype(series):
        pass
    ma = max(series)
    mi = min(series)
    if mi == ma:
        cuts = 1
    if logspaced:  # TODO fix logspaced
        if mi <= 0:
            bins = np.concatenate([[0], np.logspace(np.log(mi + 1), np.log(ma), cuts - 1)])
        else:
            bins = np.logspace(np.log(mi), np.log(ma), cuts)
    else:
        bins = np.linspace(mi, ma, cuts)
    return bins


def bin_dynamically(series, normed=False, logspaced=False):
    series = series[series.notna()]  # TODO maybe problematic?
    length = len(series)
    bins = get_dynamic_bins(series, logspaced)
    binned = pd.cut(series, bins)
    mids = bins[:-1] + np.diff(bins) * .5
    counts = series.groupby(binned).size()  # .rename_axis('intervals').reset_index()
    if normed:
        counts /= length
    return mids, counts.values


def binned_ecdf(series):
    mids, freqs = bin_dynamically(series, normed=True)
    return mids, np.cumsum(freqs)


def sample_empirical(series, n):
    mids, cumfreqs = binned_ecdf(series)
    r = np.random.random((1, n))
    return mids[np.argmax(r <= cumfreqs[:, np.newaxis], axis=0) - 1]


def crosscorr(datax, datay, lag=0, wrap=False):
    """ Lag-N cross correlation.
    Shifted data filled with NaNs

    Parameters
    ----------
    lag : int, default 0
    datax, datay : pandas.Series objects of equal length
    Returns
    ----------
    crosscorr : float
    """
    if wrap:
        shiftedy = datay.shift(lag)
        shiftedy.iloc[:lag] = datay.iloc[-lag:].values
        return datax.corr(shiftedy)
    else:
        return datax.corr(datay.shift(lag))


def lagged_corr(s1, s2, max_lag=30):
    rs = np.array([crosscorr(s1, s2, lag) for lag in range(-max_lag, max_lag + 1)])
    good_lags = np.argsort(-1 * rs) - max_lag
    offset = good_lags[0]
    f, ax = plt.subplots(figsize=(14, 3))
    ax.plot(range(-max_lag, max_lag + 1), rs)
    ax.axvline(0, color='k', linestyle='--', label='Center')
    ax.axvline(np.argmax(rs) - max_lag, color='r', linestyle='--', label='Peak synchrony')
    ax.set(
        title=f'Offset = {offset} [p_r={rs[good_lags[0] + max_lag]:.2f}] periods (others: {",".join(map(str, good_lags[1:4]))})\nS1 leads <> S2 leads',
        xlabel='Offset', ylabel='Pearson r')
    # ax.set_xticks([0, 50, 100, 151, 201, 251, 301])
    # ax.set_xticklabels([-150, -100, -50, 0, 50, 100, 150]);
    plt.legend()


def resample_in_n_out(df, freq='d'):
    ins = pd.get_dummies(df.source, prefix='source').groupby(df.entry).sum().resample(freq).sum()
    outs = pd.get_dummies(df.resource, prefix='resource').groupby(df.exit).sum().resample(freq).sum()
    shared_start = max(ins.index[0], outs.index[0])
    shared_end = min(ins.index[-1], outs.index[-1])
    return ins.loc[shared_start:shared_end], outs.loc[shared_start:shared_end]


def reverse_sort(sorting):
    reverse = np.empty_like(sorting)
    reverse[sorting] = np.arange(sorting.size)
    return reverse


def burstiness(series):
    o = series.std()
    m = series.mean()
    return (o - m) / (o + m)


