from collections import Counter

import pandas as pd

import sim.viz
from sim.manual_modeling import ModelBuilder
from sim.simulation import simulate, create_simulation_model


def test_assignment_propensities():
    mb = ModelBuilder()
    mb.add_activity('A', 'R', 2, initial_activity=True, final_activity=True)
    graph, config = mb.build()
    print(config)
    sim.viz.view(sim.viz.visualize_sim_graph(graph))

    model = create_simulation_model(graph, config)
    simulation = simulate(model)

    df = pd.DataFrame(data=((ce.activity, ce.resource) for c in simulation.generated_cases for ce in c),
                         columns=['activity', 'resource'])
    df.groupby('activity').count().plot.hist('resource', subplots=True)