import matplotlib.pyplot as plt
import numpy as np
import hmmlearn
from hmmlearn import hmm

from queue_model import *


def do_hmm(vals, num_states=2):
    vals = np.reshape(vals, (-1, 1))
    ghm = hmm.GaussianHMM(n_components=num_states, n_iter=100).fit(vals)
    score, hidden_states = ghm.decode(vals)
    print('log likelihood', score)
    idx = np.argsort(np.squeeze(ghm.means_))
    hidden_states = list(map(lambda i: idx[i], hidden_states))
    plt.plot(vals)
    plt.twinx()
    plt.plot(hidden_states, color='red')
    plt.show()
    return score


def agg_hmm(q: Queue, freq='W'):
    df = q.get_data()
    if not df.empty:
        entry_ts = pd.DataFrame(index=pd.DatetimeIndex(pd.to_datetime(df.entry, utc=True)),
                                data={'source': df.source.values}).groupby(pd.Grouper(freq=freq)).size()
        entry_ts.sort_index(inplace=True)
        exits_ts = pd.DataFrame(index=pd.DatetimeIndex(pd.to_datetime(df.exit, utc=True)),
                                data={'resource': df.resource.values}).groupby(pd.Grouper(freq=freq)).size()
        exits_ts.sort_index(inplace=True)
        plt.figure(figsize=(15, 5))
        plt.subplot(121)
        do_hmm(entry_ts.values)
        plt.title('aggregated entries hmm')
        plt.subplot(122)
        do_hmm(exits_ts.values)
        plt.title('aggregated exits hmm')
        plt.show()
