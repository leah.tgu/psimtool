from queue_model import *
from importer import do_log, log_dict
from queue_stats import *


def run():

    for log in log_dict:

        print(log)
        queues = do_log('fine')

        for n, q in queues.items():
            print(n)
            tags = classify_qr(q) + classify_ias(q) + classify_df(q)
            print(tags)

