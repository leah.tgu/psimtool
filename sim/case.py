from __future__ import annotations

import datetime
from collections import Sequence
from dataclasses import dataclass, field
from typing import Callable, Any, List, Iterator, Dict

import sim.utils


@dataclass(unsafe_hash=True)
class CaseEvent:
    activity: str
    resource: str
    timestamp: datetime.datetime
    lifecycle: str = 'complete'
    attributes: sim.utils.FrozenDict[str, Any] = field(default_factory=sim.utils.FrozenDict)

    def __init__(self, activity: str, resource: str, time: datetime.datetime, lifecycle: str = 'complete',
                 **attributes) -> None:
        self.activity = activity
        self.resource = resource
        self.timestamp = time
        self.lifecycle = lifecycle
        self.attributes = sim.utils.FrozenDict(attributes)

    def __str__(self) -> str:
        return f'{self.activity}[{self.lifecycle}] @{self.timestamp.strftime("%Y-%m-%d %H:%M")} by {self.resource}]'


# TODO StaticHashable usage, see sim_model
class Case(Sequence):

    def __init__(self, case_id: str, events: List[CaseEvent] = None, **case_attributes):
        super(Case, self).__init__()  # hash_obj=case_id)
        self.case_id = case_id
        self.attributes = case_attributes
        self.events = events if events else []

    def __getitem__(self, i: int) -> CaseEvent:
        return self.events[i]

    def index(self, x: Any, start: int = ..., end: int = ...) -> int:
        return self.events.index(x, start, end)

    def count(self, x: Any) -> int:
        return self.events.count(x)

    def __contains__(self, x: object) -> bool:
        return x in self.events

    def __iter__(self) -> Iterator[CaseEvent]:
        return iter(self.events)

    def __reversed__(self) -> Iterator[CaseEvent]:
        return reversed(self.events)

    def __len__(self) -> int:
        return len(self.events)

    def add_event(self, event: CaseEvent) -> None:
        self.events.append(event)

    def project(self, condition: Callable[[CaseEvent], bool] = lambda ce: ce.lifecycle == 'complete',
                projection: Callable[[CaseEvent], Any] = lambda ce: ce.activity):
        projection = projection if projection else lambda ce: ce
        condition = condition if condition else lambda ce: True
        return Case(self.case_id, [projection(event) for event in self.events if condition(event)], **self.attributes)

    def filter(self, condition: Callable[[CaseEvent], bool]):
        return Case(self.case_id, [event for event in self.events if condition(event)], **self.attributes)

    def __str__(self) -> str:
        return f'Case {self.case_id}: ' + ','.join(map(str, self.events))


