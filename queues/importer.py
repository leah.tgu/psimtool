import matplotlib.pyplot as plt

import queue_adapter

from pm4py.objects.log.importer.xes import importer
from pm4py.statistics.traces.log import case_statistics
from pm4py.algo.filtering.log.variants import variants_filter
from pm4py.objects.log.util.sampling import sample_log as log_sampler
from pm4py.algo.discovery.dfg import algorithm as dfg_miner
from pm4py.algo.discovery.inductive import algorithm as inductive_miner
from pm4py.visualization.dfg import visualizer as dfg_vis
from pm4py.visualization.petrinet import visualizer as net_vis
import os

from pm4py.algo.discovery.heuristics import algorithm as heuristic_miner
from pm4py.visualization.heuristics_net import visualizer as heuristic_vis

basedir_viz = 'viz'
basedir_log = '../event_logs'

log_dict = {  # 'copier': os.path.join('synthetic', 'copier.xes'),
    'fine': os.path.join(basedir_log, 'reallife', 'Road_Traffic_Fine_Management_Process.xes.gz'),
    'hospital': os.path.join(basedir_log, 'reallife', 'Hospital Billing - Event Log.xes.gz'),
    # 'driftfreq': os.path.join(basedir_log,'concept drift', 'fr', 'fr5k.MXML'),
    # 'driftcontrol': os.path.join(basedir_log,'concept drift', 'cd', 'cd5k.MXML'),
    'payment': os.path.join(basedir_log, 'reallife', 'bpi', 'RequestForPayment.xes'),
    'apm': os.path.join(basedir_log, 'reallife', 'bpi', 'APM_A2b.xes'),
    'bpi12': 'C:/Users/Daniel/Documents/Event event_logs/BPI_Challenge_2012.xes',
    'bpi17o': os.path.join(basedir_log, 'reallife', 'bpi', 'BPI Challenge 2017 - Offer log.xes.gz'),
    'sim': os.path.join(basedir_log, 'sim', 'sim.xes'),
    'sim2': os.path.join(basedir_log, 'sim', 'sim2.xes'),
    'sim_seq': os.path.join(basedir_log, 'sim', 'testruns', 'sequential.xes'),
    'sim_seq_m': os.path.join(basedir_log, 'sim', 'testruns', 'sequential_modified.xes'),
    'sim3': os.path.join(basedir_log, 'sim', 'sim3.xes')
}


def load_log(name):
    path = log_dict[name]
    log = importer.apply(path)
    return log


def filter_log(log, retain_pct=.8):
    filtered_log = variants_filter.filter_log_variants_percentage(log, percentage=retain_pct)
    return filtered_log


def sample_log(log, n):
    return log_sampler(log, n) if len(log) > n else log


def prepare_log(name, sample=True, filter_heavily=True):
    log = filter_log(load_log(name), 0.2 if filter_heavily else 0.8)
    return sample_log(log, 10000) if sample else log


def log_overview(log, name='log', petri=False, heuristic=False):
    length = len(log)
    print(name, '\tcases:', length)

    print('trace attributes')
    print(log[0].attributes)
    print('event attributes')
    print(log[0][0])

    var_with_count = case_statistics.get_variant_statistics(log, parameters={"max_variants_to_return": 5})

    for variant in var_with_count:
        print(variant['count'] / length, variant['variant'])

    dfg = dfg_miner.apply(log)
    dfg_gviz = dfg_vis.apply(dfg, log)
    dfg_vis.view(dfg_gviz)
    plt.show()

    net, initial, final = None, None, None
    if petri:
        net, initial, final = inductive_miner.apply(log)
        net_gviz = net_vis.apply(net, initial_marking=initial, final_marking=final, log=log, variant='performance')
        net_vis.view(net_gviz)
        plt.show()

    heu_net = None
    if heuristic:
        heu_net = heuristic_miner.apply_heu(log,
                                            parameters={'dfg_pre_cleaning_noise_thresh': .15, 'min_dfg_occurrences': 1,
                                                        'min_act_count': 100, 'and_measure_thresh': .6})
        heu_viz = heuristic_vis.apply(heu_net)
        heuristic_vis.view(heu_viz)
        plt.show()

    return {'heuristic': heu_net, 'petri': (net, initial, final), 'dfg': dfg}


def do_log(name):
    log = prepare_log(name, sample=True)
    models = log_overview(log, name, petri=True)
    return mine_queues(log, models['petri'])


def mine_queues(log, model):
    queue_dict = queue_adapter.derive_using_alignments(log, model)

    # Parallel(n_jobs=10)(queue.get_replay() for queue in queue_dict.values())

    for activity, queue in queue_dict.items():
        # r = queue.get_replay()
        pass

    return {activity: queue for activity, queue in queue_dict.items() if not queue.is_empty()}
