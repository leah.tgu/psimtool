import os
from datetime import datetime, timedelta

import numpy

import sim.parameter_implementations as pimpls
import sim.time_utils
from sim import simulation, exporting, viz
from sim.manual_modeling import GraphBuilder, ModelBuilder, split_of, join_of
from sim.model_configuration import ArrivalProcessConfig, ActivityConfig, ResourceConfig, DecisionConfig, MappingConfig, \
    ModelConfiguration


def sequential_graph():
    gb = GraphBuilder()
    gb.add_activity('A', initial_node=True)
    gb.add_activity('B', final_node=True)
    gb.connect('A', 'B')
    return gb.build()


def goto_graph():
    gb = GraphBuilder()
    gb.add_activities('A', 'B', 'C', 'D', 'E')
    gb.add_decision('decision1')
    gb.add_concurrency()
    gb.set_as_initial('A')
    gb.connect('A', 'and1_split')
    gb.connect('and1_split', ['decision1_split', 'D'])
    gb.connect('decision1_split', ['B', 'C'])
    gb.connect(['B', 'C'], 'decision1_join')
    gb.connect('decision1_join', 'and1_join')
    gb.connect('D', 'and1_join')
    gb.connect('and1_join', 'E')
    gb.set_as_final('E')
    return gb.build()


def sequential_config():
    dic = {
        'arrivals': {'default': ArrivalProcessConfig(datetime(2021, 4, 17, 13, 15), pimpls.ExpSampler(60),
                                                     pimpls.StandardWorkweek)},
        'activities':
            {'A': ActivityConfig(pimpls.Fifo, pimpls.ExpSampler(90), pimpls.AlwaysInBusiness),
             'B': ActivityConfig(pimpls.Fifo, pimpls.ExpSampler(60), pimpls.AlwaysInBusiness)},
        'decisions': {},
        'resources':
            {'R1': ResourceConfig(1, pimpls.AlwaysInBusiness, pimpls.PeakPerformance),
             'R2': ResourceConfig(1, pimpls.AlwaysInBusiness, pimpls.PeakPerformance)},
        'ar_mapping': MappingConfig(
            {'A': {'R1'},
             'B': {'R2'}})
    }
    return ModelConfiguration(**dic)


def goto_config():
    dic = {
        'arrival': {'default': ArrivalProcessConfig(datetime(2021, 4, 17, 13, 15), pimpls.ExpSampler(70),
                                                    pimpls.StandardWorkweek)},
        'activities':
            {'A': ActivityConfig(pimpls.Fifo, pimpls.ExpSampler(50), pimpls.AlwaysInBusiness),
             'B': ActivityConfig(pimpls.Fifo, pimpls.ExpSampler(40), pimpls.StandardWorkweek),
             'C': ActivityConfig(pimpls.Fifo, pimpls.ExpSampler(50), pimpls.StandardWorkweek),
             'D': ActivityConfig(pimpls.Fifo, pimpls.ExpSampler(45), pimpls.StandardWorkweek),
             'E': ActivityConfig(pimpls.Fifo, pimpls.ExpSampler(30), pimpls.AlwaysInBusiness)},
        'decisions': {'decision1': DecisionConfig(pimpls.StochasticClassifier([.5, .5]))},
        'resources':
            {'System': ResourceConfig(2, pimpls.AlwaysInBusiness, pimpls.PeakPerformance),
             'R1': ResourceConfig(2, pimpls.StandardWorkweek, pimpls.PeakPerformance),
             'R2': ResourceConfig(1, pimpls.StandardWorkweek, pimpls.PeakPerformance)},
        'ar_mapping': MappingConfig(
            {'A': {'System'},
             'B': {'R1'},
             'C': {'R1'},
             'D': {'R2'},
             'E': {'System'}
             })
    }
    return ModelConfiguration(**dic)


def sequential_model():
    mb = ModelBuilder()
    mb.set_default_arrival_config(
        ArrivalProcessConfig(datetime(2021, 4, 17, 13, 15), pimpls.ExpSampler(60), pimpls.StandardWorkweek))
    mb.add_activity('A', ActivityConfig(pimpls.Fifo, pimpls.ZeroSampler, pimpls.AlwaysInBusiness), 'R1',
                    initial_node=True)
    mb.add_activity('B', ActivityConfig(pimpls.Fifo, pimpls.ExpSampler(90), pimpls.AlwaysInBusiness), 'R2')
    mb.add_activity('C', ActivityConfig(pimpls.Fifo, pimpls.ExpSampler(60), pimpls.AlwaysInBusiness), 'R3',
                    final_node=True)
    mb.set_config('R1', ResourceConfig(100, pimpls.AlwaysInBusiness, pimpls.PeakPerformance))
    mb.set_config('R2', ResourceConfig(1, pimpls.AlwaysInBusiness, pimpls.PeakPerformance))
    mb.set_config('R3', ResourceConfig(1, pimpls.AlwaysInBusiness, pimpls.PeakPerformance))
    mb.connect('A', 'B')
    mb.connect('B', 'C')
    return mb.build()


def sequential_model_modified():
    sg, cfg = sequential_model()
    cfg.resources['R2'] = ResourceConfig(2, pimpls.AlwaysInBusiness, pimpls.PeakPerformance)
    return sg, cfg


def goto_model():
    mb = ModelBuilder()
    mb.set_default_arrival_config(
        ArrivalProcessConfig(datetime(2021, 4, 17, 13, 15), pimpls.ExpSampler(70), pimpls.StandardWorkweek))
    mb.add_resource('System', ResourceConfig(2, pimpls.AlwaysInBusiness, pimpls.PeakPerformance))
    mb.add_resource('R1', ResourceConfig(2, pimpls.StandardWorkweek, pimpls.PeakPerformance))
    mb.add_resource('R2', ResourceConfig(1, pimpls.StandardWorkweek, pimpls.PeakPerformance))
    mb.add_activity('A', ActivityConfig(pimpls.Fifo, pimpls.ExpSampler(50), pimpls.AlwaysInBusiness), 'System',
                    initial_node=True)
    mb.add_activity('B', ActivityConfig(pimpls.Fifo, pimpls.ExpSampler(50), pimpls.AlwaysInBusiness), 'R1')
    mb.add_activity('C', ActivityConfig(pimpls.Fifo, pimpls.ExpSampler(40), pimpls.StandardWorkweek), 'R1')
    mb.add_activity('D', ActivityConfig(pimpls.Fifo, pimpls.ExpSampler(50), pimpls.StandardWorkweek), 'R2')
    mb.add_activity('E', ActivityConfig(pimpls.Fifo, pimpls.ExpSampler(45), pimpls.StandardWorkweek), 'System',
                    final_node=True)
    mb.add_decision('decision1', DecisionConfig(pimpls.StochasticClassifier([.5, .5])))
    mb.add_concurrency()
    mb.connect('A', 'and1_split')
    mb.connect('and1_split', ['decision1_split', 'D'])
    mb.connect('decision1_split', ['B', 'C'])
    mb.connect(['B', 'C'], 'decision1_join')
    mb.connect('decision1_join', 'and1_join')
    mb.connect('D', 'and1_join')
    mb.connect('and1_join', 'E')
    return mb.build()

def running_example():
    mb = ModelBuilder()
    mb.set_default_arrival_config(
        ArrivalProcessConfig(datetime(2021, 10, 10, 6, 0), pimpls.ExpSampler(70), pimpls.StandardWorkweek))
    mb.add_resource('R1', ResourceConfig(1, pimpls.StandardWorkweek))
    mb.add_resource('R2', ResourceConfig(2, pimpls.StandardWorkweek))
    mb.add_resource('R3', ResourceConfig(2, pimpls.StandardWorkweek))
    mb.add_activity('A', ActivityConfig(pimpls.Fifo, pimpls.ExpSampler(10)), 'R1',
                    initial_node=True)
    mb.add_activity('B', ActivityConfig(pimpls.Fifo, pimpls.ExpSampler(30)), 'R2')
    mb.add_activity('C', ActivityConfig(pimpls.Fifo, pimpls.ExpSampler(60)), 'R2')
    mb.add_activity('D', ActivityConfig(pimpls.Fifo, pimpls.ExpSampler(45)), 'R3')
    mb.add_activity('E', ActivityConfig(pimpls.Fifo, pimpls.ExpSampler(15)), 'R1',
                    final_node=True)
    mb.add_decision('decision1', DecisionConfig(pimpls.StochasticClassifier([.75, .25])))
    mb.add_concurrency()
    mb.connect('A', 'and1_split')
    mb.connect('and1_split', ['decision1_split', 'D'])
    mb.connect('decision1_split', ['B', 'C'])
    mb.connect(['B', 'C'], 'decision1_join')
    mb.connect('decision1_join', 'and1_join')
    mb.connect('D', 'and1_join')
    mb.connect('and1_join', 'E')
    return mb.build()



def another_one():
    mb = ModelBuilder()
    mb.set_default_arrival_config(
        ArrivalProcessConfig(datetime(2021, 4, 17, 13, 15), pimpls.StaticSampler(timedelta(hours=1)),
                             pimpls.AlwaysInBusiness))
    mb.add_resource('System', ResourceConfig(2, pimpls.AlwaysInBusiness, pimpls.PeakPerformance))
    mb.add_resource('R1', ResourceConfig(1, pimpls.AlwaysInBusiness, pimpls.PeakPerformance))
    mb.add_resource('R2', ResourceConfig(1, pimpls.AlwaysInBusiness, pimpls.PeakPerformance))
    mb.add_activity('A', ActivityConfig(pimpls.Fifo, pimpls.ExpSampler(50), pimpls.AlwaysInBusiness), 'System',
                    initial_node=True)
    mb.add_activity('B', ActivityConfig(pimpls.Fifo, pimpls.ExpSampler(20), pimpls.AlwaysInBusiness), 'R1')
    mb.add_activity('C', ActivityConfig(pimpls.Fifo, pimpls.ExpSampler(40), pimpls.AlwaysInBusiness), 'R2')
    and_node = mb.add_concurrency(final_node=True)
    mb.connect('A', split_of(and_node))
    mb.connect(split_of(and_node), ['B', 'C'])
    mb.connect(['B', 'C'], join_of(and_node))
    return mb.build()


def delay_model():
    mb = ModelBuilder()
    R = mb.add_resource('R', ResourceConfig(numpy.inf))
    A = mb.add_activity('A',
                        ActivityConfig(queueing_discipline=pimpls.Fifo,
                                       processing_time_sampler=pimpls.ZeroSampler),
                        assignable_resources=R, initial_node=True)
    B = mb.add_activity('B',
                        ActivityConfig(queueing_discipline=pimpls.Fifo,
                                       processing_time_sampler=pimpls.ZeroSampler,
                                       delay_sampler=pimpls.StaticSampler(timedelta(hours=1))),
                        assignable_resources=R, final_node=True)
    mb.connect(A, B)
    return mb.build()

def dst():
    mb = ModelBuilder()
    mb.set_arrival_configs({'baseline': ArrivalProcessConfig(datetime(2021, 10, 31, tzinfo=sim.time_utils.get_tz()), pimpls.ExpSampler(30), last_arrival=datetime(2021, 12, 31, tzinfo=sim.time_utils.get_tz())),
                            'peak': ArrivalProcessConfig(datetime(2021, 11, 1, tzinfo=sim.time_utils.get_tz()),
                                                             pimpls.ExpSampler(10), last_arrival=datetime(2021, 12, 1,
                                                                                                          tzinfo=sim.time_utils.get_tz())),
                            })
    R = mb.add_resource('R', ResourceConfig(numpy.inf))
    A = mb.add_activity('A',
                        ActivityConfig(queueing_discipline=pimpls.Fifo,
                                       processing_time_sampler=pimpls.StaticSampler(timedelta(minutes=5))),
                        assignable_resources=R, initial_node=True)
    B = mb.add_activity('B',
                        ActivityConfig(queueing_discipline=pimpls.Fifo ,
                                       processing_time_sampler=pimpls.StaticSampler(timedelta(minutes=20)),
                                       delay_sampler=pimpls.StaticSampler(timedelta(minutes=15))),
                        assignable_resources=R, final_node=True)
    mb.connect(A, B)
    return mb.build()

def par():
    mb = ModelBuilder()
    mb.set_default_arrival_config(
        ArrivalProcessConfig(datetime(2021, 4, 17, 13, 15), pimpls.ExpSampler(30), pimpls.StandardWorkweek))
    A = mb.add_activity('a', ActivityConfig(pimpls.Fifo, pimpls.StaticSampler(timedelta(minutes=30)), pimpls.AlwaysInBusiness), ['r'], resource_propensities=[2])
    B = mb.add_activity('b', ActivityConfig(pimpls.Fifo, pimpls.StaticSampler(timedelta(minutes=30)), pimpls.AlwaysInBusiness), ['r'], resource_propensities=[1])
    mb.set_config('r', ResourceConfig(1, pimpls.AlwaysInBusiness, pimpls.PeakPerformance))
    #mb.set_config('R2', ResourceConfig(1, pimpls.AlwaysInBusiness, pimpls.PeakPerformance))
    and_node = mb.add_concurrency(initial_node=True, final_node=True)
    mb.connect(split_of(and_node), [A, B])
    mb.connect([A, B], join_of(and_node))
    return mb.build()


def run_test(sg, cfg, name='default_test', viz_graph=False):
    if viz_graph:
        dirty_vis(sg)
    print(cfg)
    sm = simulation.create_simulation_model(sg, cfg, simulation.default_execution_parameters())
    print(sm)
    simulator = simulation.simulate(sm, simulation_log_filename=os.path.join('testruns', f'{name}.log'))
    print(f'simulation took {simulator.duration}')
    # for c in simulator.generated_cases:
    #    print(str(c))
    log = simulator.get_log()
    exporting.save_log(log, os.path.join('sim', 'testruns', f'{name}.xes'))

def dirty_vis(sg):
    dot = viz.visualize_sim_graph(sg)
    viz.save(dot, 'dirty_test_vis.png')

